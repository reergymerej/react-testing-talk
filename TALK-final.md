The idea of testing is great.

There is no shortage of pep rally/gospel regarding testing your code.  That's
most of what you run into, why testing is great.  OK, you're sold.  Now what?
Easy, there are plenty of "How to set up Jest/Mocha/Tape/Jasmine/XYZ."

There's no need to sell you on these ideas,
as you're obviously interested in testing.


---

Here is a breakdown of the resources you'll generally find regarding testing
software.

1.  Propoganda / Pep Rally / Gospel
2.  Setup Instructions
3.  API Docs
4.  Philosophy

The first sells you the idea. It explains that testing insulates an app from
accidental breakage, provides confidence to developers, acts as a form of
documentation that doesn't get stale, and even changes the way you approach a
problem, leading to superior code.

The second and third categories are essentially reference manuals for how to use
the tool you've selected to implement your master plan.

The Philosophy category is fun.  It contains pearls of wisdom and lots of
opinions about what should be tested, how it should be tested, and why.  This is
the stuff hardcore nerds can really get into.  It is usually well thought out
and pretty compelling, written by testing gurus who seem to have all the answers
and exprience.

You're probably familiar with these resources, considering you're interested in
a talk about testing React.  You're probably also familiar with the challenge of
trying to apply this collective knowledge to your daily work.

Here's a dirty little secret.
